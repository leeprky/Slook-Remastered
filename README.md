# SLOOK (REMASTERED)
A Discord Theme Based **Around The Chat**, Making it more appealing.

![preview]()

## How To Install SLOOK Remastered?

Its Simple!

For **Powercord** Users:

1. Open CMD & Type:

```
cd powercord/src/Powercord/themes && git clone https://github.com/leeprky/Slook-Remastered
```

For **Betterdiscord** Users:

CLick > [Download]()


For **Goosemod** Users:

1. Navigate To The Themes Section & Filter By My Name *Leeprky* And Click Install.

# Extras 

Thank you for checking out/downloading my Theme :)
Direct Message Me On @Discord Or Join By **[github.com/leeprky](https://discord.gg/Ff3rqAYB89)** For More Help/Information For Installing/Support. Enjoy :)

Thanks For The Overwhelming Support On SLOOK V2 :)

## Previews

